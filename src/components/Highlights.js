import { Row, Col, Card, Button, Form } from 'react-bootstrap';
import img1 from '../images/bannercat.jpg';
import { useState } from "react";



export default function Highlights() {
    const [showMore, setShowMore] =useState (false);
    
    const toggleShowMore = () => {
        setShowMore(!showMore);
    };
    
    return (
        <Row className="mt-3 mb-3">
            <div className='image-container'>
                <img src ={img1} alt= "" className='img-fluid'/>
            </div>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3" bg="light">
                    <Card.Body>
                        <Card.Title className="text-center">
                            <h3>Explore our extensive range of cat food options to cater to all dietary requirements of your feline companion</h3>
                        </Card.Title>
                        <Card.Text className="text-center">
                        No matter what your budget or your cat's health needs are, we have got you covered. At Pawsitively Purrfect, we offer a wide range of cat food options, from dry kibble to canned wet food, to make shopping for your cat's food simple and convenient. You can easily shop by brand or browse our collection of best-selling and featured cat food items to find the perfect option for your feline companion.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3" bg="light">
                    <Card.Body>
                        <Card.Title className="text-center">
                            <h3>Enhance the quality of your cat's diet</h3>
                        </Card.Title>
                        <Card.Text className="text-center">
                        A healthy and balanced diet is crucial to maintaining your cat's overall health and well-being. To ensure that your cat is getting the best possible nutrition, it is important to choose cat food that is high in proteins, minerals, enzymes, amino acids, and other essential ingredients. At our store, we offer a wide range of cat food options from popular brands such as Whiskas, Sheba, Royal Canin, Friskies, and more, including both wet and dry cat food options.
                        <br/>
                        <br/>
                        Different cat food brands and products are designed to  
                        {showMore ? (
                            <>
                            
                              cater to various life stages, from kittens to adult and senior cats.   For example, you can choose dry cat food for kittens to meet their unique nutritional requirements. Additionally, our cat food collection includes highly nutritious options that are formulated to address specific health needs, such as Royal Canin's health and care nutrition food lines for the cat's immune, urinary, hair, and skin health.
                            <br />
                            
                            <Button variant="link" onClick={toggleShowMore}>
                            Read less
                            </Button>
                            </>
                        ) : (
                            <Button variant="link" onClick={toggleShowMore}>
                            Read more
                            </Button>
                        )}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3" bg="light">
                    <Card.Body>
                        <Card.Title className="text-center">
                            <h3>Benefits of Interactive Cat Toys.</h3>
                        </Card.Title>
                        <Card.Text className="text-center">
                        Interactive cat toys offer a wide range of benefits for your feline companion. These toys not only provide physical exercise but also stimulate your cat's mind, reducing boredom and promoting mental wellness. By engaging in interactive play, cats can develop their natural instincts, improve their agility and coordination, and enhance their hunting skills.
                        <br/>
                        <br/>
                        At our store, we offer a wide range of interactive cat toys cater to various life stages, from kittens to adult and senior cats. For example,
                        {showMore ? (
                            <>
                            
                                you can choose dry cat food for kittens to meet their unique nutritional requirements. Additionally, our cat food collection includes highly nutritious options that are formulated to address specific health needs, such as Royal Canin's health and care nutrition food lines for the cat's immune, urinary, hair, and skin health.
                           
                           
                            <Button variant="link" onClick={toggleShowMore}>
                            Read less
                            </Button>
                            </>
                        ) : (
                            <Button variant="link" onClick={toggleShowMore}>
                            Read more
                            </Button>
                        )}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Row className="mt-5 justify-content-center">
                <Col xs={12} md={4} className="text-center">
                    <h1>Join the Cat-Loving Shop!</h1>
                    <p>Be the first to get our latest updates, cat health resources, and more.</p>
                    <Form>
                   
                    <Button variant="warning" href="http://localhost:3000/register" target="_blank">
                        Join the family!
                    </Button>
                    
                    </Form>
                </Col>
            </Row>
            <Row className="mt-5 justify-content-center">
                <Col xs={12} md={8} className="text-md-center">
                   <p>Pawsitively Purrfect, your go-to shop for cat care, provides educational content on cat health that is carefully crafted and reviewed by our team of veterinary experts. Our content is aligned with the most up-to-date evidence-based veterinary information and health guidelines. It's important to note that our educational content is not intended to replace consultation with a qualified veterinarian and should not be taken as veterinary advice. By accessing our site, you agree to our terms of use and privacy policy. ©2023 Multiverse Media Ltd.</p>
                   
                </Col>
            </Row>
                
            
        </Row>
        

        
        
    )
}
