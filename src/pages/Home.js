import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights' ;


export default function Home() {

const data = {
    title: "Pawsitively Purrfect",
    content: "Pawsitively Purrfect is the ultimate cat grooming experience. Our expert groomers are trained to pamper your feline friend, providing them with a relaxing and enjoyable spa day. From a luxurious bath to a trim and shape, our grooming services are designed to enhance your cat's natural beauty and promote their overall health and well-being.",
    
}

	return (
		<Fragment>
            <Banner data = {data}/>
            <Highlights />
		</Fragment>
	)
}